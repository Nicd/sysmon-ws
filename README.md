Sysmon-ws is a simple Python WebSocket server which broadcasts system
information to all listening clients. It also includes a small HTML5 UI which
displays the information in a somewhat sensible manner. I pieced this together
one night when inspiration hit, so the code is written in a quick and dirty
manner.

Sysmon-ws uses 3rd party code:

* AutoBahnPython WebSocketServer from <https://github.com/tavendo/AutobahnPython/blob/master/examples/websocket/broadcast/server.py>
* jquery-graceful-websocket from <https://code.google.com/p/jquery-graceful-websocket/>


### Installation

Clone the files to a directory, create a virtualenv if you like and then
install libs with pip:

    pip install -r requirements.txt


### Usage

Usage example from my server (I use a virtualenv for it):

    [nicd@nytsoi ~]$ cd /srv/sysmon-ws
    [nicd@nytsoi sysmon-ws]$ . venv-sysmon-ws/bin/activate
    (venv-sysmon-ws)[nicd@nytsoi sysmon-ws]$ python server.py >/dev/null


### License

* Original AutoBahnPython code in server.py is licensed with Apache 2.0, read
  server.py for more details.
* jquery-graceful-websocket (ws.js) is licensed with the MIT License.
* My code (index.html and modifications in server.py) is licensed with the
  MIT License. See LICENSE for more details.
